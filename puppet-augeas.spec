Autoreq: 0

Name:           puppet-augeas
Version:        1.0
Release:        5%{?dist}
Summary:        Puppetlabs augeas module

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tgz
BuildArch:      noarch
Requires:       puppet-agent

%description
Puppetlabs augeas module.

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/augeas/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/augeas/

%files -n puppet-augeas
%{_datadir}/puppet/modules/augeas
%doc code/README.md

%changelog
* Tue Jan 16 2024 CERN Linux Droid <linux.ci@cern.ch> - 1.0-5
- Rebased to #9ac8ab00c86c257825068ab0a86ae7a8e84f1e73 by locmap-updater

* Fri Jan 12 2024 CERN Linux Droid <linux.ci@cern.ch> - 1.0-4
- Rebased to #9ac8ab00c86c257825068ab0a86ae7a8e84f1e73 by locmap-updater

* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> 1.0-3
- Bump release for disttag change

* Thu Feb 03 2022 Ben Morrice <ben.morrice@cern.ch> - 1.0-2
- remove empty common.yaml (avoiding warnings when running in verbose)

* Fri Sep 17 2021 Daniel Juarez <djuarezg@cern.ch> - 1.0-1
- Initial release
